var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
System.register("Vehicle", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("Bike", [], function (exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var Bike;
    return {
        setters: [],
        execute: function () {
            Bike = (function () {
                function Bike() {
                    this.weight = 5;
                    this.price = 500;
                }
                Bike.prototype.start = function () {
                    throw new Error("the bike dosn't need to start !");
                };
                Bike.prototype.stop = function () {
                    throw new Error("the bike stopped brutally !");
                };
                Bike.prototype.move = function (d) {
                    console.log("the bike moved " + d + "meters ");
                };
                return Bike;
            }());
            exports_2("Bike", Bike);
        }
    };
});
System.register("Car", [], function (exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var Car;
    return {
        setters: [],
        execute: function () {
            Car = (function () {
                function Car(id, brand, model, horsePower, weight, price, nbSeats) {
                    this.id = id;
                    this.brand = brand;
                    this.model = model;
                    this.horsePower = horsePower;
                    this.weight = weight;
                    this.price = price;
                    this.nbSeats = nbSeats;
                    this._isRunning = false;
                }
                Car.prototype.start = function () {
                    if (this._isRunning) {
                        console.log("this cas is already started");
                        return false;
                    }
                    else {
                        console.log("car is starting...");
                        this._isRunning = true;
                        return true;
                    }
                };
                Car.prototype.stop = function () {
                    if (!this._isRunning) {
                        console.log("this car is already stopped");
                        return false;
                    }
                    else {
                        console.log("car stopping...");
                        this._isRunning = false;
                        return true;
                    }
                };
                Car.prototype.move = function (d) {
                    if (this._isRunning) {
                        console.log("this car moved " + d + "meters.");
                    }
                    else {
                        console.log("this car isn't started !");
                    }
                };
                return Car;
            }());
            exports_3("Car", Car);
        }
    };
});
System.register("SmartCar", ["Car", "Bike"], function (exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var Car_1, Bike_1, SmartCar, car, bike, smartCar;
    return {
        setters: [
            function (Car_1_1) {
                Car_1 = Car_1_1;
            },
            function (Bike_1_1) {
                Bike_1 = Bike_1_1;
            }
        ],
        execute: function () {
            SmartCar = (function (_super) {
                __extends(SmartCar, _super);
                function SmartCar() {
                    return _super.call(this, 0, "Google", "unknown", 90, 800, 20000, 1) || this;
                }
                SmartCar.prototype.move = function (distance) {
                    _super.prototype.move.call(this, distance);
                    console.log("la voiture intelligente avance plus rapidement");
                };
                return SmartCar;
            }(Car_1.Car));
            exports_4("SmartCar", SmartCar);
            car = new Car_1.Car(1, "Peugeot", "407", 120, 1700, 14000, 5);
            car.move(5);
            car.start();
            car.move(20);
            car.stop();
            bike = new Bike_1.Bike();
            bike.move(2);
            smartCar = new SmartCar();
            smartCar.start();
            smartCar.move(40);
        }
    };
});
//# sourceMappingURL=tsc.js.map