import {Vehicle} from "./Vehicle";
import {Car } from "./Car";
import {Bike} from "./Bike";

export class SmartCar extends Car {
    constructor(){
        super(0, "Google","unknown", 90, 800, 20000, 1);
    }
    move(distance: number): void{
        super.move(distance);
        console.log("la voiture intelligente avance plus rapidement");
    }
}
let car: Car = new Car(1, "Peugeot","407",120,1700,14000,5);
car.move(5);
car.start();
car.move(20);
car.stop();

let bike: Bike = new Bike();
bike.move(2);

let smartCar: SmartCar = new SmartCar();
smartCar.start();
smartCar.move(40);