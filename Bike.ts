import { Vehicle} from "./Vehicle";

export class Bike implements Vehicle{
    id : number;
    weight: number;
    price: number;
    nbSeats : number;
    constructor(){
        this.weight = 5;
        this.price= 500;
    }

    start(): boolean{
        throw new Error("the bike dosn't need to start !");
    }
    stop(): boolean{
        throw new Error("the bike stopped brutally !");
    }
    move (d: number): void {
        console.log("the bike moved "+ d+ "meters ");
    }
}